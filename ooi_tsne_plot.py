from time import time

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.ticker import NullFormatter
import seaborn as sns

from sklearn import manifold, datasets

import pandas as pd

DIR =  "/Users/qybo123/PycharmProjects/Recom_sys/OOI/ooi_2019/"
FILE = "ooi_all_split_2019.csv"
n_sample = 500
n_frac = 0.2

df_all = pd.read_csv(DIR + FILE)
df_all_fac = df_all[["userIP", "site", "node", "ins"]].apply(lambda x: pd.factorize(x)[0])
df_all_narray = df_all_fac[["userIP", "site", "node", "ins"]].to_numpy()

# df_small_fac = df_all_fac.sample(n=n_sample)
df_small_fac = df_all_fac.sample(frac=n_frac)
df_small_narray = df_small_fac[["userIP", "site", "node", "ins"]].to_numpy()

user_cnt = df_small_fac.userIP.nunique()
print(f"user={user_cnt}")

# %matplotlib widget

fig = plt.figure(figsize=(8, 8))

n_num = int(len(df_small_fac)*n_frac)

plt.title("%i dataset t-SNE" %n_num)

t0 = time()
tsne = manifold.TSNE(n_components=2, init='pca', verbose=1, method='barnes_hut')
Y = tsne.fit_transform(df_small_narray)
t1 = time()
print("t-SNE: %.2g sec" % (t1 - t0))  # 算法用时

# plt.scatter(Y[:, 0], Y[:, 1])
userID_list = df_small_narray[:, 0]
df_tsne = pd.DataFrame({'X_axis': Y[:, 0], "Y_axis": Y[:, 1], "userID": userID_list})

# sns.scatterplot(x="X_axis", y="Y_axis", data=df_tsne)
sns.scatterplot(x="X_axis", y="Y_axis", data=df_tsne, \
                hue=userID_list, \
                palette=sns.color_palette("hls", user_cnt),\
               legend=False)

plt.show()
# plt.savefig(DIR + "ooi_tsne_" + str(n_sample)+"_plot.png")
plt.savefig(DIR + "ooi_tsne_" + str(n_frac*100)+"_percent_plot.png")