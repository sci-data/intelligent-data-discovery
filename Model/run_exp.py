import os
from time import time

################################
# Test different knowledge
################################
# suff_list = ["_md", "_sp", "_dkg", "_md_sp", "_md_dkg", "_sp_dkg", "_md_sp_dkg"]
suff_list = ["_sp", "_dkg", "_sp_dkg", "_uu", "_uu_sp_dkg", "_uu_md_sp_dkg"]

# t0 = time()
# for suff in suff_list:
# 	print(f"Currnt OOI suff {suff} ==============================")
# 	os.system("python Main.py --model_type kgat --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.0001 --epoch 2 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 2048 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

# print(f"OOI use time = {time() - t0}")

t1 = time()

for suff in suff_list:
	print(f"Currnt GAGE suff {suff} ==============================")
	os.system("python Main.py --model_type kgat --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.0001 --epoch 1 --verbose 1 --save_flag 1 --pretrain -1 --batch_size 4096 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

print(f"GAGE use time = {time() - t1}")

print("DONE!")


################################
# Test different layer
################################

# suff_list = ["_sp", "_dkg", "_sp_dkg", "_uu", "_uu_sp_dkg", "_uu_md_sp_dkg"]




# t0 = time()

# for suff in suff_list:

# # print(f"Currnt OOI ATT on ==============================")
# 	print(f"current {suff}")
# 	os.system("python Main.py --model_type kgat --alg_type graphsage --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.0001 --epoch 10 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format())


# print(f"Currnt OOI 1 layer ==============================")
# os.system("python Main.py --model_type kgat --alg_type graphsage --dataset ooi --regs [1e-5,1e-5] --layer_size [64] --embed_size 64 --lr 0.0001 --epoch 10 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _sp_dkg")

# print(f"Currnt OOI 2 layer ==============================")
# os.system("python Main.py --model_type kgat --alg_type graphsage --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32] --embed_size 64 --lr 0.0001 --epoch 10 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _sp_dkg")

# print(f"Currnt OOI ATT on ==============================")
# os.system("python Main.py --model_type kgat --alg_type graphsage --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.0001 --epoch 10 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _sp_dkg")

# print(f"Currnt OOI ATT off ==============================")
# os.system("python Main.py --model_type kgat --alg_type graphsage --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.0001 --epoch 10 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att False --use_kge True --suff _sp_dkg")

# print(f"Currnt OOI 4 layer ==============================")
# os.system("python Main.py --model_type kgat --alg_type graphsage --dataset ooi --regs [1e-5,1e-5] --layer_size [128,64,32,16] --embed_size 64 --lr 0.0001 --epoch 10 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _sp_dkg")



# print(f"OOI use time = {time() - t0}")

# t1 = time()

# print(f"Currnt GAGE 1 layer ==============================")
# os.system("python Main.py --model_type kgat --alg_type graphsage --dataset unavco --regs [1e-5,1e-5] --layer_size [64] --embed_size 64 --lr 0.0001 --epoch 10 --verbose 5 --save_flag 1 --pretrain -1 --batch_size 2048 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _sp_dkg")

# print(f"Currnt GAGE 2 layer ==============================")
# os.system("python Main.py --model_type kgat --alg_type graphsage --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32] --embed_size 64 --lr 0.0001 --epoch 10 --verbose 5 --save_flag 1 --pretrain -1 --batch_size 2048 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _sp_dkg")

# print(f"Currnt GAGE ATT on ==============================")
# os.system("python Main.py --model_type kgat --alg_type graphsage --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.0001 --epoch 10 --verbose 5 --save_flag 1 --pretrain -1 --batch_size 2048 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _sp_dkg")


# print(f"Currnt GAGE ATT off ==============================")
# os.system("python Main.py --model_type kgat --alg_type graphsage --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.0001 --epoch 10 --verbose 5 --save_flag 1 --pretrain -1 --batch_size 2048 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att False --use_kge True --suff _sp_dkg")

# print(f"Currnt GAGE 4 layer ==============================")
# os.system("python Main.py --model_type kgat --alg_type graphsage --dataset unavco --regs [1e-5,1e-5] --layer_size [128,64,32,16] --embed_size 64 --lr 0.0001 --epoch 10 --verbose 5 --save_flag 1 --pretrain -1 --batch_size 2048 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _sp_dkg")


# print(f"GAGE use time = {time() - t1}")

print("DONE!")