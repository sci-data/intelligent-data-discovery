import os
from time import time


################################
# 
# OOI
#
################################


# ################################
# # Test different knowledge
# ################################
# # suff_list = ["_md", "_sp", "_dkg", "_md_sp", "_md_dkg", "_sp_dkg", "_md_sp_dkg"]
# suff_list = ["_md", "_sp", "_dkg", "_sp_dkg", "_uu", "_uu_sp_dkg", "_uu_md_sp_dkg"]

# print("OOI == All Knolwedge ==============")

# t0 = time()
# for suff in suff_list:
# 	print(f"Currnt OOI suff {suff} ==============================")
# 	os.system("python Main.py --model_type kgat --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.005 --epoch 10 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

# print(f"OOI all knowledge use time = {time() - t0}")



# ################################
# # Test all models
# ################################
# t0 = time()
# suff = "_uu_sp_dkg"
# print(f"Currnt OOI BPRMF ==============================")
# os.system("python Main.py --model_type bprfm --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.01 --epoch 10 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

# print(f"Currnt OOI FM ==============================")
# os.system("python Main.py --model_type fm --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.01 --epoch 10 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

# print(f"Currnt OOI NFM ==============================")
# os.system("python Main.py --model_type nfm --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.05 --epoch 10 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

# print(f"Currnt OOI CKE ==============================")
# os.system("python Main.py --model_type kgat --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.01 --epoch 10 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

# print(f"Currnt OOI CFKG ==============================")
# os.system("python Main.py --model_type cfkg --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.05 --epoch 10 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

# print(f"Currnt OOI KGAT ==============================")
# os.system("python Main.py --model_type kgat --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.005 --epoch 10 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

# print(f"OOI all model use time = {time() - t0}")



# ################################
# # Test different layer
# ################################
# t0 = time()
# suff = "_uu_sp_dkg"
# print(f"Currnt OOI 1 layer ==============================")
# os.system("python Main.py --model_type kgat --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64] --embed_size 64 --lr 0.0051 --epoch 10 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

# print(f"Currnt OOI 2 layer ==============================")
# os.system("python Main.py --model_type kgat --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32] --embed_size 64 --lr 0.005 --epoch 10 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

# # print(f"Currnt OOI 4 layer ==============================")
# # os.system("python Main.py --model_type kgat --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [128,64,32,16] --embed_size 64 --lr 0.0001 --epoch 10 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 2048 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))


# print(f"OOI different layers use time = {time() - t0}")


# ################################
# # Test different aggregator
# ################################
# # t0 = time()
# # suff = "_uu_sp_dkg"

# print(f"Currnt OOI KGAT bi ==============================")
# os.system("python Main.py --model_type kgat --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.005 --epoch 10 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

# print(f"Currnt OOI KGAT GCN ==============================")
# os.system("python Main.py --model_type kgat --alg_type gcn --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.005 --epoch 10 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

# print(f"Currnt OOI KGAT Graphsage ==============================")
# os.system("python Main.py --model_type kgat --alg_type graphsage --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.005 --epoch 10 --verbose 10 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))


# # print(f"OOI different aggregator use time = {time() - t0}")



# print("!!!!!!!!!! OOI DONE !!!!!!!!!!!!!!!!!!!!")

################################
# 
# GAGE
#
################################

################################
# Test different knowledge
################################
# suff_list = ["_md", "_sp", "_dkg", "_md_sp", "_md_dkg", "_sp_dkg", "_md_sp_dkg"]
suff_list = ["_md", "_sp", "_dkg", "_sp_dkg", "_uu", "_uu_sp_dkg", "_uu_md_sp_dkg"]

print("GAGE == All Knolwedge ==============")

t0 = time()
for suff in suff_list:
	print(f"Currnt GAGE suff {suff} ==============================")
	os.system("python Main.py --model_type kgat --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.005 --epoch 10 --verbose 5 --save_flag 1 --pretrain -1 --batch_size 512 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

print(f"GAGE all knowledge use time = {time() - t0}")



################################
# Test all models
################################
t0 = time()
suff = "_uu_sp_dkg"
print(f"Currnt GAGE BPRMF ==============================")
os.system("python Main.py --model_type bprfm --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.005 --epoch 10 --verbose 5 --save_flag 1 --pretrain -1 --batch_size 512 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

print(f"Currnt GAGE FM ==============================")
os.system("python Main.py --model_type fm --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.01 --epoch 10 --verbose 5 --save_flag 1 --pretrain -1 --batch_size 256 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

print(f"Currnt GAGE NFM ==============================")
os.system("python Main.py --model_type nfm --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.005 --epoch 10 --verbose 5 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

print(f"Currnt GAGE CKE ==============================")
os.system("python Main.py --model_type kgat --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.005 --epoch 10 --verbose 5 --save_flag 1 --pretrain -1 --batch_size 1024 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

print(f"Currnt GAGE CFKG ==============================")
os.system("python Main.py --model_type cfkg --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.05 --epoch 10 --verbose 5 --save_flag 1 --pretrain -1 --batch_size 256 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

# print(f"Currnt GAGE KGAT ==============================")
# os.system("python Main.py --model_type kgat --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.005 --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size 512 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

print(f"GAGE all model use time = {time() - t0}")



################################
# Test different layer
################################
t0 = time()
suff = "_uu_sp_dkg"
print(f"Currnt GAGE 1 layer ==============================")
os.system("python Main.py --model_type kgat --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64] --embed_size 64 --lr 0.005 --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size 512 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

print(f"Currnt GAGE 2 layer ==============================")
os.system("python Main.py --model_type kgat --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32] --embed_size 64 --lr 0.005 --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size 512 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

print(f"Currnt GAGE 4 layer ==============================")
os.system("python Main.py --model_type kgat --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [128,64,32,16] --embed_size 64 --lr 0.005 --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size 512 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))


print(f"GAGE different layers use time = {time() - t0}")


################################
# Test different aggregator
################################
t0 = time()
suff = "_uu_sp_dkg"

print(f"Currnt GAGE KGAT bi ==============================")
os.system("python Main.py --model_type kgat --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.005 --epoch 2 --verbose 5 --save_flag 1 --pretrain -1 --batch_size 512 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

print(f"Currnt GAGE KGAT GCN ==============================")
os.system("python Main.py --model_type kgat --alg_type gcn --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.005 --epoch 2 --verbose 5 --save_flag 1 --pretrain -1 --batch_size 512 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))

print(f"Currnt GAGE KGAT Graphsage ==============================")
os.system("python Main.py --model_type kgat --alg_type graphsage --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] --embed_size 64 --lr 0.005 --epoch 2 --verbose 5 --save_flag 1 --pretrain -1 --batch_size 512 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff {}".format(suff))


print(f"GAGE different aggregator use time = {time() - t0}")


print("!!!!!!!!!! GAGE DONE !!!!!!!!!!!!!!!!!!!!")

# print("DONE!")





