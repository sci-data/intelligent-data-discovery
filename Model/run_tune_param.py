import os
from time import time


################################
# 
# OOI
#
################################



################################
# Test different knowledge
################################
# suff =  "_uu_sp_dkg"
# batch_size_list = [512, 1024, 2048, 4098]
# lr_list = [0.05, 0.01, 0.005]

# batch_size_list = [128, 256, 512]
# lr_list = [0.05, 0.01, 0.005]


# # CFKG
# t0 = time()
# for batch_size in batch_size_list:
# 	print(f"Currnt OOI CFKG batch_size {batch_size} ==============================")
# 	os.system("python Main.py --model_type cfkg --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] \
# 			--embed_size 64 --lr 0.01 --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size {} --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(batch_size))


# for lr in lr_list:
# 	print(f"Currnt OOI CFKG lr {lr} ==============================")
# 	os.system("python Main.py --model_type cfkg --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] \
# 			--embed_size 64 --lr {} --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size 2048 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(lr))


# print(f"OOI CFKG tune parameter use time = {time() - t0}")


# # FM
# t0 = time()
# for batch_size in batch_size_list:
# 	print(f"Currnt OOI FM batch_size {batch_size} ==============================")
# 	os.system("python Main.py --model_type fm --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] \
# 			--embed_size 64 --lr 0.01 --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size {} --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(batch_size))


# for lr in lr_list:
# 	print(f"Currnt OOI CFKG lr {lr} ==============================")
# 	os.system("python Main.py --model_type fm --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] \
# 			--embed_size 64 --lr {} --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size 2048 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(lr))


# print(f"OOI FM tune parameter use time = {time() - t0}")


# # NFM
# t0 = time()
# for batch_size in batch_size_list:
# 	print(f"Currnt OOI NFM batch_size {batch_size} ==============================")
# 	os.system("python Main.py --model_type nfm --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] \
# 			--embed_size 64 --lr 0.01 --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size {} --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(batch_size))


# for lr in lr_list:
# 	print(f"Currnt OOI NFM lr {lr} ==============================")
# 	os.system("python Main.py --model_type nfm --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] \
# 			--embed_size 64 --lr {} --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size 2048 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(lr))


# print(f"OOI NFM tune parameter use time = {time() - t0}")


# # CKE
# t0 = time()
# for batch_size in batch_size_list:
# 	print(f"Currnt OOI CKE batch_size {batch_size} ==============================")
# 	os.system("python Main.py --model_type cke --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] \
# 			--embed_size 64 --lr 0.01 --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size {} --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(batch_size))


# for lr in lr_list:
# 	print(f"Currnt OOI CKE lr {lr} ==============================")
# 	os.system("python Main.py --model_type cke --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] \
# 			--embed_size 64 --lr {} --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size 2048 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(lr))


# print(f"OOI CKE tune parameter use time = {time() - t0}")

# # KGAT
# t0 = time()
# for batch_size in batch_size_list:
# 	print(f"Currnt OOI KGAT batch_size {batch_size} ==============================")
# 	os.system("python Main.py --model_type kgat --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] \
# 			--embed_size 64 --lr 0.01 --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size {} --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(batch_size))


# for lr in lr_list:
# 	print(f"Currnt OOI KGAT lr {lr} ==============================")
# 	os.system("python Main.py --model_type kgat --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] \
# 			--embed_size 64 --lr {} --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size 2048 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(lr))


# print(f"OOI KGAT tune parameter use time = {time() - t0}")


# BPRMF
# t0 = time()
# for batch_size in batch_size_list:
# 	print(f"Currnt OOI BPRMF batch_size {batch_size} ==============================")
# 	os.system("python Main.py --model_type bprmf --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] \
# 			--embed_size 64 --lr 0.01 --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size {} --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(batch_size))


# for lr in lr_list:
# 	print(f"Currnt OOI BPRMF lr {lr} ==============================")
# 	os.system("python Main.py --model_type bprmf --alg_type bi --dataset ooi --regs [1e-5,1e-5] --layer_size [64,32,16] \
# 			--embed_size 64 --lr {} --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size 2048 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(lr))


# print(f"OOI BPRMF tune parameter use time = {time() - t0}")


#############
# GAGE 
################


suff =  "_uu_sp_dkg"
# batch_size_list = [512, 1024, 2048, 4098]
# lr_list = [0.05, 0.01, 0.005]

batch_size_list = [256]
lr_list = [0.05, 0.01, 0.005, 0.001]


# CFKG
t0 = time()
for batch_size in batch_size_list:
	print(f"Currnt GAGE CFKG batch_size {batch_size} ==============================")
	os.system("python Main.py --model_type cfkg --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] \
			--embed_size 64 --lr 0.01 --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size {} --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(batch_size))


for lr in lr_list:
	print(f"Currnt GAGE CFKG lr {lr} ==============================")
	os.system("python Main.py --model_type cfkg --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] \
			--embed_size 64 --lr {} --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size 512 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(lr))


print(f"GAGE CFKG tune parameter use time = {time() - t0}")


# FM
t0 = time()
for batch_size in batch_size_list:
	print(f"Currnt GAGE FM batch_size {batch_size} ==============================")
	os.system("python Main.py --model_type fm --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] \
			--embed_size 64 --lr 0.01 --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size {} --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(batch_size))


for lr in lr_list:
	print(f"Currnt GAGE CFKG lr {lr} ==============================")
	os.system("python Main.py --model_type fm --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] \
			--embed_size 64 --lr {} --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size 512 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(lr))


print(f"GAGE FM tune parameter use time = {time() - t0}")


# NFM
t0 = time()
for batch_size in batch_size_list:
	print(f"Currnt GAGE NFM batch_size {batch_size} ==============================")
	os.system("python Main.py --model_type nfm --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] \
			--embed_size 64 --lr 0.01 --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size {} --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(batch_size))


for lr in lr_list:
	print(f"Currnt GAGE NFM lr {lr} ==============================")
	os.system("python Main.py --model_type nfm --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] \
			--embed_size 64 --lr {} --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size 512 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(lr))


print(f"GAGE NFM tune parameter use time = {time() - t0}")


# CKE
t0 = time()
for batch_size in batch_size_list:
	print(f"Currnt GAGE CKE batch_size {batch_size} ==============================")
	os.system("python Main.py --model_type cke --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] \
			--embed_size 64 --lr 0.01 --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size {} --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(batch_size))


for lr in lr_list:
	print(f"Currnt GAGE CKE lr {lr} ==============================")
	os.system("python Main.py --model_type cke --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] \
			--embed_size 64 --lr {} --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size 512 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(lr))


print(f"GAGE CKE tune parameter use time = {time() - t0}")

# KGAT
t0 = time()
for batch_size in batch_size_list:
	print(f"Currnt GAGE KGAT batch_size {batch_size} ==============================")
	os.system("python Main.py --model_type kgat --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] \
			--embed_size 64 --lr 0.01 --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size {} --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(batch_size))


for lr in lr_list:
	print(f"Currnt GAGE KGAT lr {lr} ==============================")
	os.system("python Main.py --model_type kgat --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] \
			--embed_size 64 --lr {} --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size 512 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(lr))


print(f"GAGE KGAT tune parameter use time = {time() - t0}")


# BPRMF
t0 = time()
for batch_size in batch_size_list:
	print(f"Currnt GAGE BPRMF batch_size {batch_size} ==============================")
	os.system("python Main.py --model_type bprmf --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] \
			--embed_size 64 --lr 0.01 --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size {} --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(batch_size))


for lr in lr_list:
	print(f"Currnt GAGE BPRMF lr {lr} ==============================")
	os.system("python Main.py --model_type bprmf --alg_type bi --dataset unavco --regs [1e-5,1e-5] --layer_size [64,32,16] \
			--embed_size 64 --lr {} --epoch 2 --verbose 2 --save_flag 1 --pretrain -1 --batch_size 512 --node_dropout [0.1] --mess_dropout [0.1,0.1,0.1] --use_att True --use_kge True --suff _uu_sp_dkg".format(lr))


print(f"GAGE BPRMF tune parameter use time = {time() - t0}")

print("DONE")


