'''
Created on Dec. 03, 2019
Tensorflow Implementation of the Collaborative Knowledge-aware graph ATention network (CKAT) model in:
Qin Yubo et al. Facilitating Data Discovery for Large-scale Science Facilities using Knowledge Networks. In IPDPS 2021.

Refer to:
Wang Xiang et al. KGAT: Knowledge Graph Attention Network for Recommendation. In KDD 2019.
Wang Hongwei et al. Knowledge Graph Convolutional Networks for Recommender Systems. In WWW 2019.
'''
import os


def ensureDir(dir_path):
    d = os.path.dirname(dir_path)
    if not os.path.exists(d):
        os.makedirs(d)

def early_stopping(log_value, best_value, stopping_step, expected_order='acc', flag_step=100):
    # early stopping strategy:
    assert expected_order in ['acc', 'dec']

    if (expected_order == 'acc' and log_value >= best_value) or (expected_order == 'dec' and log_value <= best_value):
        stopping_step = 0
        best_value = log_value
    else:
        stopping_step += 1

    if stopping_step >= flag_step:
        print("Early stopping is trigger at step: {} log:{}".format(flag_step, log_value))
        should_stop = True
    else:
        should_stop = False
    return best_value, stopping_step, should_stop