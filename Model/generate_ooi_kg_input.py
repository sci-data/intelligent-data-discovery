import pandas as pd
import collections,json
import os

DIR_IN = "../data/ooi/KG_generation_input/"
DIR_OUT = "../data/ooi/CKAT_input/"
FILE = "ooi_raw_data_KG_full.csv"
INPUT_FILE = "a_ooi_all_full_kg_2019_CKAT_input_no_uu.txt"

if not os.path.exists(DIR_OUT):
    os.makedirs(DIR_OUT)

item2idx_dict = collections.defaultdict()
# 包括OOI提取出来的所有input enities (refdes, some sites, array) 还有 instrument name, primary discipline, research theme
with open(DIR_IN + "ooi_item2idx_dict2.txt", "r") as input_dict:
    item2idx_dict = json.loads(input_dict.read())

refdesID_set = set()  # for generating the item_index2entity_id.txt
rtheme_set = set()
array_set = set()
dis_set = set()

with open(DIR_IN + "ooi_d2d_array_kg_35_dict.txt", "r") as input_dict:
    d2d_array_kg_dict = json.loads(input_dict.read())

with open(DIR_IN + "ooi_d2d_site_kg_158_dict.txt", "r") as input_dict:
    d2d_site_kg_dict = json.loads(input_dict.read())

with open(DIR_IN + "ooi_d2d_ins_kg_136_dict.txt", "r") as input_dict:
    d2d_ins_kg_dict = json.loads(input_dict.read())

# add additional item id to the item2idx_dict

cnt = len(item2idx_dict)
for idx in range(35):
    item2idx_dict["gArray" + str(idx)] = cnt
    cnt += 1

for idx in range(158):
    item2idx_dict["gSite" + str(idx)] = cnt
    cnt += 1

for idx in range(136):
    item2idx_dict["gIns" + str(idx)] = cnt
    cnt += 1

df_input_kg_full = pd.read_csv(DIR_IN + FILE)
# Drop duplicate row
df_input_kg_full_clean = df_input_kg_full.drop_duplicates(subset=["ref_des", "ins_serie"], keep='first')
# Add instrument primiary discipline and research theme
df_input_ins_discipline_rtheme_dtype = pd.read_csv(DIR_IN + "ins_primary_discipline_research_themes.csv")

# 构造rtheme 和 discipline dict
ins_discipline_dict = collections.defaultdict()
ins_rthem_dict = collections.defaultdict()
for idx, row in df_input_ins_discipline_rtheme_dtype.iterrows():
    ins = row["instrument_code"]
    dis = row["primary_discipline"]
    rtheme = row["research_theme"]
    if ins not in ins_discipline_dict:
        ins_discipline_dict[ins] = dis
    if ins not in ins_rthem_dict:
        ins_rthem_dict[ins] = rtheme

# Get the OOI trace
refdes_set = set()
for line in open(DIR_IN + INPUT_FILE, encoding='utf-8').readlines()[1:]:
    array = line.strip().split('\t')
    refdes_set.add(array[1])

input_refdes_list = list(refdes_set)

user_loc_list = []
with open(DIR_IN + 'ooi_user_loc_list.txt', "r") as input_file:
    user_loc_list = json.loads(input_file.read())

####################3
# Generate KG: UU+SP+DKG
####################
INPUT_FILE = "a_ooi_all_full_kg_2019_KGCN_input_w_uu.txt"
# Get the OOI trace
refdes_set = set()
for line in open(DIR_IN + INPUT_FILE, encoding='utf-8').readlines()[1:]:
    array = line.strip().split('\t')
    refdes_set.add(array[1])

input_refdes_list = list(refdes_set)

suffix = "uu_sp_dkg"

print("Processing kg_" + suffix + ".txt ....")

array_has_written_list = []
site_has_written_list = []
ins_has_written_list = []

# 将所有出现的entity id，都map成从0-n的连续的ID，这些ID都在kg.txt中
# ItemID to entityID dict
itemId2entityId_dict = collections.defaultdict()
entity_cnt = 0

for item in input_refdes_list:
    itemId2entityId_dict[int(item)] = entity_cnt
    entity_cnt += 1

# 输出 KG
with open(DIR_OUT + "kg_" + suffix + ".txt", "w+", encoding='utf-8') as output_file:
    # process refdes and array
    for idx, row in df_input_kg_full_clean.iterrows():
        refdes = row["ref_des"]
        array = row["array"]
        site = row["site_code"]
        ins = row["ins_code"]

        if refdes not in item2idx_dict:
            continue
        if array not in item2idx_dict:
            continue

        refdesItemID = item2idx_dict[refdes]
        if refdesItemID not in itemId2entityId_dict:
            continue
            # refdesId肯定在itemId2entityId_dict中
        refdesID = itemId2entityId_dict[refdesItemID]

        arrayItemID = item2idx_dict[array]
        siteItemID = item2idx_dict[site]

        insItemID = item2idx_dict[ins]

        if arrayItemID not in itemId2entityId_dict:  # scodeID is item id
            itemId2entityId_dict[arrayItemID] = entity_cnt
            entity_cnt += 1
        arrayID = itemId2entityId_dict[arrayItemID]

        if siteItemID not in itemId2entityId_dict:  # scodeID is item id
            itemId2entityId_dict[siteItemID] = entity_cnt
            entity_cnt += 1
        siteID = itemId2entityId_dict[siteItemID]

        # Consider Instrument KG as the DKG
        if insItemID not in itemId2entityId_dict:  # scodeID is item id
            itemId2entityId_dict[insItemID] = entity_cnt
            entity_cnt += 1

        insID = itemId2entityId_dict[insItemID]

        if array in d2d_array_kg_dict:
            if array not in array_has_written_list:
                array_has_written_list.append(array)

                for group in d2d_array_kg_dict[array]:
                    groupItemID = item2idx_dict[group]

                    if groupItemID not in itemId2entityId_dict:
                        itemId2entityId_dict[groupItemID] = entity_cnt
                        entity_cnt += 1
                    groupID = itemId2entityId_dict[groupItemID]

                    output_file.write("%d\t%s\t%d\n" % (arrayID, "array.has_d2d_array_kg.groupID", groupID))

        if site in d2d_site_kg_dict:
            if site not in site_has_written_list:
                site_has_written_list.append(site)

                for group in d2d_site_kg_dict[site]:
                    groupItemID = item2idx_dict[group]

                    if groupItemID not in itemId2entityId_dict:
                        itemId2entityId_dict[groupItemID] = entity_cnt
                        entity_cnt += 1
                    groupID = itemId2entityId_dict[groupItemID]

                    output_file.write("%d\t%s\t%d\n" % (siteID, "site.has_d2d_site_kg.groupID", groupID))

        # This is the DKG
        if ins in d2d_ins_kg_dict:
            if ins not in ins_has_written_list:
                ins_has_written_list.append(ins)

                for group in d2d_ins_kg_dict[ins]:
                    groupItemID = item2idx_dict[group]

                    if groupItemID not in itemId2entityId_dict:
                        itemId2entityId_dict[groupItemID] = entity_cnt
                        entity_cnt += 1
                    groupID = itemId2entityId_dict[groupItemID]

                    output_file.write("%d\t%s\t%d\n" % (insID, "ins.has_d2d_ins_kg.groupID", groupID))

        output_file.write("%d\t%s\t%d\n" % (refdesID, "refdes.is_included.array", arrayID))
        output_file.write("%d\t%s\t%d\n" % (refdesID, "refdes.is_included.site", siteID))
        output_file.write("%d\t%s\t%d\n" % (refdesID, "refdes.is.ins", insID))

        # Load user location
    for user_loc in user_loc_list:
        if user_loc not in item2idx_dict:
            continue

        user_locItemID = item2idx_dict[user_loc]

        if user_locItemID not in itemId2entityId_dict:  # scodeID is item id
            itemId2entityId_dict[user_locItemID] = entity_cnt
            entity_cnt += 1
        user_locID = itemId2entityId_dict[user_locItemID]

        output_file.write("%d\t%s\t%d\n" % (user_locID, "user_loc.is_user_group.user_loc", user_locID))

with open(DIR_OUT + "itemID_pair_used_in_rating_kg_file_" + suffix + ".txt", "w+", encoding='utf-8') as output_file:
    for itemID, entityID in itemId2entityId_dict.items():
        output_file.write("%d\t%d\n" % (itemID, entityID))  # itemID and entityID are the same
