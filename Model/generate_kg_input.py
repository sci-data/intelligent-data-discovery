'''
Created on Dec. 03, 2019
Tensorflow Implementation of the Collaborative Knowledge-aware graph ATention network (CKAT) model in:
Qin Yubo et al. Facilitating Data Discovery for Large-scale Science Facilities using Knowledge Networks. In IPDPS 2021.

Refer to:
Wang Xiang et al. KGAT: Knowledge Graph Attention Network for Recommendation. In KDD 2019.
Wang Hongwei et al. Knowledge Graph Convolutional Networks for Recommender Systems. In WWW 2019.
'''

import collections
import os
import numpy as np
import pandas as pd
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-d', type=str, default='ooi')

args = parser.parse_args()

suff_list = ["_md", "_sp", "_dkg", "_sp_dkg", "_uu", "_uu_sp_dkg", "_uu_md_sp_dkg"]
# suff_list = ["_uu", "_uu_sp_dkg", "_uu_md_sp_dkg"]

for suff in suff_list:

    print(f"Get file {'../Data/' + args.d + '/ratings' + suff + '.txt'}")

    rating_file = '../Data/' + args.d + '/ratings' + suff + '.txt'
    rating_np_org = np.loadtxt(rating_file, dtype=np.int32)

    pos_indices = []
    for idx, row in enumerate(rating_np_org):
        if row[2] == 1:
            pos_indices.append(idx)

    rating_np = rating_np_org[pos_indices]

    # Train:Test = 8:2
    test_ratio = 0.2
    n_ratings = rating_np.shape[0]

    test_indices = np.random.choice(n_ratings, size=int(n_ratings * test_ratio), replace=False)
    train_indices = list(set(range(n_ratings)) - set(test_indices))
      
    train_user_set = set()
    for idx in train_indices:
        userID = rating_np[idx][0]
        train_user_set.add(userID)

    selected_test_indices_list = []

    for idx in test_indices:
        userID = rating_np[idx][0]
        if userID in train_user_set:
            selected_test_indices_list.append(idx)

    train_data = rating_np[train_indices]
    test_data = rating_np[selected_test_indices_list]

    train_user_item_dict = collections.defaultdict(list)
    test_user_item_dict = collections.defaultdict(list)

    for idx, row in enumerate(train_data):
        userID = row[0]
        itemID = row[1]
        train_user_item_dict[userID].append(itemID)
        
    for idx, row in enumerate(test_data):
        userID = row[0]
        itemID = row[1]
        test_user_item_dict[userID].append(itemID)

    old2newUserID_dict = collections.defaultdict()
    cnt = 0
    for userID in train_user_set:
        old2newUserID_dict[userID] = cnt
        cnt += 1

    new_train_user_item_dict = collections.defaultdict(list)
    new_test_user_item_dict = collections.defaultdict(list)

    for key, val in train_user_item_dict.items():
        new_userID = old2newUserID_dict[key]
        new_train_user_item_dict[new_userID] = val
        
    for key, val in test_user_item_dict.items():
        new_userID = old2newUserID_dict[key]
        new_test_user_item_dict[new_userID] = val

    test_data_list = []
    for key, val in new_test_user_item_dict.items():
        test_data_list.append([int(key)] + [round(i) for i in val])

    with open('../Data/' + args.d + '/test' + suff + '.txt', 'w') as output:
        for val in test_data_list:
            output.write('{}\n'.format(' '.join(str(i) for i in val)))

    train_data_list = []
    for key, val in new_train_user_item_dict.items():
        train_data_list.append([int(key)] + [round(i) for i in val])

    with open('../Data/' + args.d +'/train' + suff + '.txt', 'w') as output:
        for val in train_data_list:
            output.write('{}\n'.format(' '.join(str(i) for i in val)))

print("Done!")