import pandas as pd
import collections, json

DIR = "/Users/qybo123/PycharmProjects/Recom_sys/OOI/"
DIR_RES = "/Users/qybo123/PycharmProjects/Recom_sys/OOI/figs/user_access_profile/"
# User access log
U_LOG_FILE = "ooi_100k_sample_unique_query.csv" 
# OOI_KG dict
OOI_KG_FILE = "ooi_kg_dict.txt"
ooi_kg_dict = collections.defaultdict(lambda: collections.defaultdict(list))

# Load log
df_user = pd.read_csv(DIR + U_LOG_FILE)
# Load OOI KG dict
with open(DIR + OOI_KG_FILE, "r") as ooi_kg_file:
    ooi_kg_dict = json.loads(ooi_kg_file.read())
    
# Build user access profile
# Example: ins_name, node_name, site_name, array, data_content, data_type
u_acc_dict = collections.defaultdict(lambda: collections.defaultdict(list))

for idx, row in df_user.iterrows():
    user_id = row["userID"]
    rd = row["refdes"]
    
    try:
        u_acc_dict[user_id]["ins_name"].append(ooi_kg_dict[rd]["ins_name"][0])
        u_acc_dict[user_id]["node_name"].append(ooi_kg_dict[rd]["node_name"][0].split("(")[0])
        u_acc_dict[user_id]["site_name"].append(ooi_kg_dict[rd]["site_name"][0])
        u_acc_dict[user_id]["array"].append(ooi_kg_dict[rd]["array"][0])
        u_acc_dict[user_id]["data_content"].append(ooi_kg_dict[rd]["data_content"][0])
        u_acc_dict[user_id]["data_type"].append(ooi_kg_dict[rd]["data_type"][0])
    except:
        #  会存在KG中没有的data point
        print(f"Error: {user_id} {rd}")


# Plot functions
def make_autopct(values):
    def my_autopct(pct):
        total = sum(values)
        val = int(round(pct*total/100.0))
        return '{p:.2f}%  ({v:d})'.format(p=pct,v=val)
    return my_autopct

def plot_user_analysis_data(user_id):
    global u_acc_dict
    #ins_name, node_name, site_name, array, data_content, data_type
    # Plot ins_name
    fig = plt.figure(figsize=(15, 10))
    fig.subplots_adjust(hspace=0.4, wspace=0.4)
    u_cnt = collections.Counter(u_acc_dict[str(user_id)]['ins_name'])
    ax = fig.add_subplot(2,3,1)
    patches, texts, _ = plt.pie([int(v) for v in u_cnt.values()], autopct=make_autopct(u_cnt.values()))
    plt.legend(patches, u_cnt.keys(), loc='best')
    ax.set_title("ins_name", fontsize=16)

    # Plot node_name
    u_cnt = collections.Counter(u_acc_dict[str(user_id)]['node_name'])
    ax = fig.add_subplot(2,3,2)
    patches, texts, _ = plt.pie([int(v) for v in u_cnt.values()], autopct=make_autopct(u_cnt.values()))
    plt.legend(patches, u_cnt.keys(), loc='best')
    ax.set_title("node_name", fontsize=16)

    # Plot site_name
    u_cnt = collections.Counter(u_acc_dict[str(user_id)]['site_name'])
    ax = fig.add_subplot(2,3,3)
    patches, texts, _ = plt.pie([int(v) for v in u_cnt.values()], autopct=make_autopct(u_cnt.values()))
    plt.legend(patches, u_cnt.keys(), loc='best')
    ax.set_title("site_name", fontsize=16)

    # Plot array
    u_cnt = collections.Counter(u_acc_dict[str(user_id)]['array'])
    ax = fig.add_subplot(2,3,4)
    patches, texts, _ = plt.pie([int(v) for v in u_cnt.values()], autopct=make_autopct(u_cnt.values()))
    plt.legend(patches, u_cnt.keys(), loc='best')
    ax.set_title("array", fontsize=16)

    # Plot data_content
    u_cnt = collections.Counter(u_acc_dict[str(user_id)]['data_content'])
    ax = fig.add_subplot(2,3,5)
    patches, texts, _ = plt.pie([int(v) for v in u_cnt.values()], autopct=make_autopct(u_cnt.values()))
    plt.legend(patches, u_cnt.keys(), loc='best')
    ax.set_title("data_content", fontsize=16)

    # Plot data_type
    u_cnt = collections.Counter(u_acc_dict[str(user_id)]['data_type'])
    ax = fig.add_subplot(2,3,6)
    patches, texts, _ = plt.pie([int(v) for v in u_cnt.values()], autopct=make_autopct(u_cnt.values()))
    plt.legend(patches, u_cnt.keys(), loc='best')
    ax.set_title("data_type", fontsize=16)

    fig.suptitle(str(user_id), fontsize=18)
    fig.savefig(DIR_RES + str(user_id)+'_profile.png')
#     plt.show()
        
# Plot result
for user_id in u_acc_dict:
    plot_user_analysis_data(user_id)
    
print("DONE!")